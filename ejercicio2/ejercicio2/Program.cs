﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ejercicio2
{
    class Program
    {
        static void Main(string[] args)
        {

            TextReader leer;
            Console.Clear();
            leer = new StreamReader("archivo.txt");
            string palabras = leer.ReadToEnd();
            leer.Close();
            char[] separador = { ' ', ',' };
            int c = 0;
            string[] partes = palabras.Split(separador);
            Stack<String> ejercicio2 = new Stack<string>();
            for (int i = 0; i < partes.Length; i++)
            {
                if (partes[i].Length < 6 )
                {
                    ejercicio2.Push(partes[i]);
                    c++;
                }
            }
            for (int i = 0; i < c; i++)
            {
                Console.WriteLine(ejercicio2.Pop());
            }
            Console.ReadLine();
           
        }
    }
}
